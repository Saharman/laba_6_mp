﻿using CommandLaba.Commands;
using CommandLaba.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandLaba
{
    public class Invoker
    {
        private Dictionary<string, ICommand<int>> Commands { get; } = new Dictionary<string, ICommand<int>>();

        private Stack<string> History { get; } = new Stack<string>();

        public void Register(string name, ICommand<int> command) {
            Commands.Add(name, command);
        }

        public void DefaultRegistration() {
            Number num = new Number();
            Register("add", new AddCommand(num));
            Register("sub", new SubtractCommand(num));
            Register("mul", new MultiplyCommand(num));
            Register("div", new DivideCommand(num));
            Register("set", new SetCommand(num));
        }

        public void Invoke(string com) {
            com = com.Trim().ToLower();
            if (com == "undo") {
                if (History.Count > 0)
                    Undo(History.Pop());
                else
                    Console.WriteLine("No commands to undo!");
            } else if (com == "redo") {
                if (History.Count > 0)
                    Execute(History.Peek());
                else
                    Console.WriteLine("No commands to redo!");
            } else
                Execute(com);
        }

        private void Undo(string com) {
            string[] parts = com.Split(' ');
            parts = parts.Where(x => !String.IsNullOrWhiteSpace(x) && !String.IsNullOrEmpty(x)).ToArray();
            ICommand<int> command = Commands[parts[0]];
            command.Undo(Int32.Parse(parts[1]));
        }

        private void Execute(string com) {
            string[] parts = com.Split(' ');
            parts = parts.Where(x => !String.IsNullOrWhiteSpace(x) && !String.IsNullOrEmpty(x)).ToArray();
            ICommand<int> command = Commands.GetValueOrDefault(parts[0]);
            if (parts.Length != 2) {
                Console.WriteLine("Wrong command!");
                return;
            }
            if (command != null) {
                try {
                    command.Execute(Int32.Parse(parts[1]));
                } catch {
                    Console.WriteLine("Wrong second arg!");
                }
                
                History.Push(com);
            } else
                Console.WriteLine("No command found!");
        }
    }
}
