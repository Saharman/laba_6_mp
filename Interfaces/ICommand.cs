﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommandLaba.Interfaces
{
    public interface ICommand<T>
    {
        void Execute(T arg);
        void Undo(T arg);
    }
}
