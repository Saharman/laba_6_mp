﻿using CommandLaba.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommandLaba.Commands
{
    public class SetCommand : ICommand<int>
    {
        private Number Number { get; set; }

        public SetCommand(Number num) =>
            Number = num;

        public void Execute(int a) =>
            Number.Set(a);

        public void Undo(int a) =>
            Number.Set(0);
    }
}
