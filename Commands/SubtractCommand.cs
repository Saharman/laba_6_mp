﻿using CommandLaba.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommandLaba.Commands
{
    public class SubtractCommand : ICommand<int>
    {
        private Number Number { get; set; }

        public SubtractCommand(Number num) =>
            Number = num;

        public void Execute(int a) =>
            Number.Subtract(a);

        public void Undo(int a) =>
            Number.Add(a);
    }
}
