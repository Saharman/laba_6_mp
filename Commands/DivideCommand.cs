﻿using CommandLaba.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommandLaba.Commands
{
    public class DivideCommand : ICommand<int>
    {
        private Number Number { get; set; }

        public DivideCommand(Number num) =>
            Number = num;

        public void Execute(int a) =>
            Number.Divide(a);

        public void Undo(int a) =>
            Number.Multiply(a);
    }
}
