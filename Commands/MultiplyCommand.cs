﻿using CommandLaba.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommandLaba.Commands
{
    public class MultiplyCommand : ICommand<int>
    {
        private Number Number { get; set; }

        public MultiplyCommand(Number num) =>
            Number = num;

        public void Execute(int a) =>
            Number.Multiply(a);

        public void Undo(int a) =>
            Number.Divide(a);
    }
}
