﻿using CommandLaba.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommandLaba.Commands
{
    public class AddCommand: ICommand<int>
    {
        private Number Number { get; set; }

        public AddCommand(Number num) =>
            Number = num;

        public void Execute(int a) =>
            Number.Add(a);

        public void Undo(int a) =>
            Number.Subtract(a);
    }
}
