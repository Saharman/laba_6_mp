﻿using System;

namespace CommandLaba
{
    class Program
    {
        static void Main(string[] args) {
            string com = Console.ReadLine();
            Invoker inv = new Invoker();
            inv.DefaultRegistration();
            while (!String.IsNullOrEmpty(com)) {
                inv.Invoke(com);
                com = Console.ReadLine();
            }

        }
    }
}
