﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommandLaba
{
    public class Number
    {
        public int Value { get; private set; } = 0;

        public void Add(int a) {
            Value += a;
            Show();
        }

        public void Subtract(int a) {
            Value -= a;
            Show();
        }

        public void Set(int a) {
            Value = a;
            Show();
        }

        public void Multiply(int a) {
            Value *= a;
            Show();
        }

        public void Divide(int a) {
            Value /= a;
            Show();
        }

        public void Show() =>
            Console.WriteLine($"Number = {Value}");
    }
}
